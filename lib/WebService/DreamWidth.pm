package WebService::DreamWidth;

use Moose;
use JSON::MaybeXS;
use Carp;

use WebService::DreamWidth::User;
use WebService::DreamWidth::Journal;
use WebService::DreamWidth::Tag;
use JSON::MaybeXS;

use MooseX::LazyFetch;


with 'Role::REST::Client';

has "api_key" =>
(
  is => 'rw',
  isa => 'Str',
  trigger => sub {my ($self, $key) = @_; $self->set_persistent_header("Authorization","Bearer $key")},
);

sub user
{
  my $self = shift;
  my $user_name = shift;

  my $res = WebService::DreamWidth::User->new(name=>$user_name, _client => $self);
  $res->fetch();
  return $res;
}


sub journal
{
  my $self = shift;
  my $name = shift;

  return WebService::DreamWidth::Journal->new(name =>$name, _client => $self);
}





sub _journal_entry
{
  my $self = shift;
  my $journal_name = shift;
  my $entry_id = shift;

  my $res = $self->get("/api/v1/journals/$journal_name/entries/$entry_id");
  $self->_my_http_die($res) if $res->code !=200;

  return WebService::DreamWidth::Entry->new(json_data => $res->response->content, journal_name => $journal_name, _client => $self);
}




sub _fetch_by_ref
{
  my $self = shift;
  my $obj = shift;

  if (ref $obj eq 'WebService::DreamWidth::User')
  {
    return($self->_fetch_user($obj->name))
  }
  elsif (ref $obj eq 'WebService::DreamWidth::Icon')
  {
    return($self->_fetch_users_icon($obj->username, $obj->picid))
  }
  elsif (ref $obj eq 'WebService::DreamWidth::Tag')
  {
    my $data = decode_json ($self->_fetch_journals_tags($obj->journal_name));
    my $siblings = $obj->_siblings() || {};
    my $res = undef;

    foreach my $tag_raw (@$data)
    {
      if ($tag_raw->{name} eq $obj->name)
      {
        $res = $tag_raw;
      } else
      {
        my $sib = $siblings->{$tag_raw->{name}};
        if ($sib && $sib->needs_fetchig)
        {
          $sib->_upfetch($tag_raw);
        }
      }
    }
    die "Tag ".$obj->name." is not found in journal ".$obj->journal_name unless $res;
    return $res;
  } else
  {
    die "Unkown ref: ".ref $obj;
  }
}

sub _fetch_user
{
  my $self = shift;
  my $user_name = shift;
  my $res = $self->get("/api/v1/users/$user_name");
  $self->_my_http_die($res) if $res->code !=200;

  return $res->response->content;
}

sub _fetch_users_icon
{
  my $self = shift;
  my $user_name = shift;
  my $picid = shift;
  my $res = $self->get("/api/v1/users/$user_name/icons/$picid");
  $self->_my_http_die($res) if $res->code !=200;

  return $res->response->content;
}

sub _fetch_users_icons
{
  my $self = shift;
  my $user_name = shift;
  my $res = $self->get("/api/v1/users/$user_name/icons");
  $self->_my_http_die($res) if $res->code !=200;

  return $res->response->content;
}


sub _users_icons
{
  my $self = shift;
  my $str = $self->_fetch_users_icons(@_);
  my $arr = decode_json($str);
  my $res = {};
  foreach my $i_data (@$arr)
  {
    my $picid = $i_data->{picid};
    $res->{$picid} =  WebService::DreamWidth::Icon->new(json_data => $i_data, _client => $self);
  }
  return $res;
}


sub _fetch_users_journals
{
  my $self = shift;
  my $user_name = shift;
  my $res = $self->get("/api/v1/users/$user_name/journals");
  $self->_my_http_die($res) if $res->code !=200;

  return $res->response->content;
}

sub _users_journals
{
  my $self = shift;
  my $str = $self->_fetch_users_journals(@_);
  my $arr = decode_json($str);
  my $res = {};
  foreach my $key (@$arr)
  {
    $res->{$key} =  WebService::DreamWidth::Journal->new(name => $key, _client => $self);
  }
  return $res;
}

sub _fetch_users_subscriptions
{
  my $self = shift;
  my $user_name = shift;
  my $res = $self->get("/api/v1/users/$user_name/subscriptions");
  $self->_my_http_die($res) if $res->code !=200;

  return $res->response->content;
}

sub _users_subscriptions
{
  my $self = shift;
  my $str = $self->_fetch_users_subscriptions(@_);
  my $arr = decode_json($str);
  my $res = {};
  foreach my $key (@$arr)
  {
    $res->{$key} =  WebService::DreamWidth::Journal->new(name => $key, _client => $self);
  }
  return $res;
}

sub _fetch_users_grants
{
  my $self = shift;
  my $user_name = shift;
  my $res = $self->get("/api/v1/users/$user_name/grants");
  $self->_my_http_die($res) if $res->code !=200;

  return $res->response->content;
}

sub _users_grants
{
  my $self = shift;
  my $str = $self->_fetch_users_grants(@_);
  my $arr = decode_json($str);
  my $res = {};
  foreach my $key (@$arr)
  {
    $res->{$key} =  WebService::DreamWidth::Journal->new(name => $key, _client => $self);
  }
  return $res;
}


sub _fetch_journals_tags
{
  my $self = shift;
  my $journal_name = shift;
  my $res = $self->get("/api/v1/journals/$journal_name/tags");
  $self->_my_http_die($res) if $res->code !=200;

  return $res->response->content;
}

sub _journals_tags
{
  my $self = shift;
  my $journal_name = shift;
  my $str = $self->_fetch_journals_tags($journal_name);
  my $arr = decode_json($str);


  my $res = {};
  foreach my $h (@$arr)
  {
    my $key = $h->{name};
    $h->{journal_name} = $journal_name;
    $h->{_client} = $self;
    $res->{$key} =  WebService::DreamWidth::Tag->new($h);
  }
  return $res;
}



sub _my_http_die
{
  my $self = shift;
  my $response = shift;
  my $content = $response->response->content;

  my $error_text = "Error fetching http data";
  eval {
    my $json = decode_json $content;
    $error_text.=": ".$json->{error} if $json->{error};
  };

  croak "$error_text (".$response->code.")";
}


=cut
use REST::Client;

has "client" =>
(
  is => 'ro',
  isa => 'REST::Client',
  lazy => 1,
  builder => '_client_builder',
);

sub _client_builder
{
  my $self = shift;
  my $client = REST::Client->new(host => $self->host);
  my $key = $self->api_key;
  $client->addHeader("Authorization","Bearer $key");
  return $client;
}

has "api_key" =>
(
  is => 'rw',
  isa => 'Str',
  trigger => sub {my ($self, $key) = @_; $self->client->addHeader("Authorization","Bearer $key")},
);

has "host" =>
(
  is => 'rw',
  isa => 'Str',
  default => "https://www.dreamwidth.org",
  trigger => sub {my ($self, $host) = @_; $self->client->setHost($host)},
);

=cut

1;