package WebService::DreamWidth::Icon;

use strict;

use Moose;
use MooseX::Embody::JSON;
use MooseX::LazyFetch;

with 'WebService::DreamWidth::Fetcher::Role';

#FIXME: add this attr
#      "keywords"

j_attr "comment" =>  (is => 'ro', lazy_fetch => 1);
j_attr "picid" =>    (is => 'ro', required => 1);
j_attr "url" =>      (is => 'ro', lazy_fetch => 1);
j_attr "username" => (is => 'ro', required => 1);


1;