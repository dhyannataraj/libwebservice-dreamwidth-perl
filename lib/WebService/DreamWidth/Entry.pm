package WebService::DreamWidth::Entry;

use Moose;
use MooseX::Embody::JSON;
use MooseX::LazyFetch;
use Moose::Util::TypeConstraints;

use WebService::DreamWidth::Icon;
use WebService::DreamWidth::Poster;
use WebService::DreamWidth::Tag;

with 'WebService::DreamWidth::Fetcher::Role';

subtype 'my_tags_hash_type'  # FIXME may be there is a better way to register HashRef[WebService::DreamWidth::Tag] as a valid type for coersion
    => as 'HashRef[WebService::DreamWidth::Tag]';

coerce 'HashRef[WebService::DreamWidth::Tag]',
    from  'ArrayRef[Str]',
    via  sub
    {
      my $tag_names = $_;
      my $res = {};
      foreach my $tag_name (@$tag_names)
      {
        $res->{$tag_name} = "WebService::DreamWidth::Tag"->new(name=>$tag_name, journal_name=>"", _siblings => $res);  # "" will be replaced in on_procreate
      }
      return $res;
    };

j_hash "tags" => (is => 'ro', class => "WebService::DreamWidth::Tag");

j_attr "journal_name" => (is => 'ro', required => 1);
j_attr "entry_id"     => (is => 'ro', required => 1);

j_attr "security"     => (is => 'ro');
j_attr "body_html"    => (is => 'ro');
j_attr "subject_raw"  => (is => 'ro');
j_attr "subject_html" => (is => 'ro');
j_attr "allowmask"    => (is => 'ro');
j_attr "body_raw"     => (is => 'ro');
j_attr "icon_keyword" => (is => 'ro');
j_attr "datetime"     => (is => 'ro');
j_attr "url"          => (is => 'ro');

j_obj "icon"    => (class => "WebService::DreamWidth::Icon",   is =>'ro');
j_obj "poster"  => (class => "WebService::DreamWidth::Poster", is =>'ro');



# Infect newly created child Tags with journal_name value
around 'on_procreate' => sub{
  my $orig = shift;
  my $self = shift;

  my $new_obj = shift; # the only argument

  if (ref $new_obj eq "WebService::DreamWidth::Tag" && $new_obj->journal_name eq "")
  {
    # Hack journal_name into readonly attribute
    $new_obj->meta->get_attribute('journal_name')->set_value($new_obj, $self->journal_name);
  }
  my $res = $self->$orig($new_obj);
  return $res;
};


1;