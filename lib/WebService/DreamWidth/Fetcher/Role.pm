package WebService::DreamWidth::Fetcher::Role;

use Moose::Role;


has '_client' =>
(
  is => 'rw',
#  isa => 'WebService::DreamWidth',
);


# Infect newly created child with _client reference
around 'on_procreate' => sub{
  my $orig = shift;
  my $self = shift;

  my $new_obj = shift; # the only argument

  my $res = $self->$orig($new_obj);
  if (defined $self->_client) # FIXME change to if attribute set moose check
  {
    $new_obj->_client($self->_client);
  }
  return $res;
};


around 'fetcher_implementation' => sub {
    my $orig = shift;
    my $self = shift;

    my $res = $self->_client->_fetch_by_ref($self);

    $res = $self->BUILDARGS($res);  # Convert args from json-text to hash and do other transformation that are designated
    return $res;
};


1;