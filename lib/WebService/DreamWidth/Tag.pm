package WebService::DreamWidth::Tag;

use strict;

use Moose;
use MooseX::Embody::JSON;
use MooseX::LazyFetch;

with 'WebService::DreamWidth::Fetcher::Role';

#FIXME: add this attr
#      "security_counts"

j_attr "name"         =>  (is => 'ro', required => 1);
j_attr "journal_name" =>  (is => 'ro', required => 1);
j_attr "url"          =>  (is => 'ro', lazy_fetch => 1);
j_attr "visibility"   =>  (is => 'ro', lazy_fetch => 1);
j_attr "use_count"    =>  (is => 'ro', lazy_fetch => 1);

has _siblings => (is => 'ro', weak_ref => 1); # FIXME restrict it to hash

1;