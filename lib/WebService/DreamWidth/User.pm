package WebService::DreamWidth::User;

use strict;

use Moose;
use MooseX::Embody::JSON;
use MooseX::LazyFetch;


with 'WebService::DreamWidth::Fetcher::Role';

# This module will finally be replaced with WebService::DreamWidth::User, I hope...

j_attr "username" => (is => 'ro');
j_attr "display_name" => (is => 'ro');

j_attr "name"              => (is => 'ro', required => 1);

j_attr "tags_count"        => (is => 'ro', lazy_fetch =>1);
j_attr "journal_title"     => (is => 'ro', lazy_fetch =>1);
j_attr "bio"               => (is => 'ro', lazy_fetch =>1);
j_attr "account_type"      => (is => 'ro', lazy_fetch =>1);
j_attr "created"           => (is => 'ro', lazy_fetch =>1);
j_attr "memories_count"    => (is => 'ro', lazy_fetch =>1);
j_attr "entries_count"     => (is => 'ro', lazy_fetch =>1);
j_attr "userid"            => (is => 'ro', lazy_fetch =>1);
j_attr "comments_posted"   => (is => 'ro', lazy_fetch =>1);
j_attr "last_updated"      => (is => 'ro', lazy_fetch =>1);
j_attr "comments_recieved" => (is => 'ro', lazy_fetch =>1);
j_attr "journal_subtitle"  => (is => 'ro', lazy_fetch =>1);


sub icons
{
  my $self = shift;
  return $self->_client->_users_icons($self->name);
}

sub journals
{
  my $self = shift;
  return $self->_client->_users_journals($self->name);
}

sub subscriptions
{
  my $self = shift;
  return $self->_client->_users_subscriptions($self->name);
}

sub grants
{
  my $self = shift;
  return $self->_client->_users_grants($self->name);
}


1;