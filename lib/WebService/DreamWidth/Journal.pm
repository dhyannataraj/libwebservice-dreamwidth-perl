package WebService::DreamWidth::Journal;

use strict;

use Moose;
use MooseX::Embody::JSON;
use MooseX::LazyFetch;


with 'WebService::DreamWidth::Fetcher::Role';

has 'name' => (is => 'rw', required => 1);

sub entry
{
  my $self = shift;
  my $entry_id = shift;
  my $entry = $self->_client->_journal_entry($self->name, $entry_id);
  return $entry;
}

sub tags
{
  my $self = shift;
  my $tags = $self->_client->_journals_tags($self->name);
  return $tags;
}

1;