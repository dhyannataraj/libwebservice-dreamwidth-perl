package WebService::DreamWidth::Poster;

use strict;

use Moose;
use MooseX::Embody::JSON;
use MooseX::LazyFetch;

with 'WebService::DreamWidth::Fetcher::Role';


# This module will finally be replaced with WebService::DreamWidth::User, I hope...

j_attr "username" => (is => 'ro');
j_attr "display_name" => (is => 'ro');


1;