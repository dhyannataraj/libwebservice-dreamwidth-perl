package MooseX::LazyFetch;

use Moose::Exporter;
use Scalar::Util qw(blessed);
# ABSTRACT: easy aliasing of methods and attributes in Moose


my %metaroles = (
    class_metaroles => {
#        class     => ['MooseX::Aliases::Meta::Trait::Class'],
#  class=>          ['MooseX::LazyFetch::Role'],

        attribute => ['MooseX::LazyFetch::Meta::Trait::Attribute'],
    },
    base_class_roles => ['MooseX::LazyFetch::Role'],
#        application_to_class =>
#            ['MooseX::LazyFetch::Role'],
#            ['MooseX::Aliases::Meta::Trait::Role::ApplicationToClass'],
#        application_to_role =>
#            ['MooseX::LazyFetch::Role'],
#
#            ['MooseX::Aliases::Meta::Trait::Role::ApplicationToRole'],
#        applied_attribute =>
#            ['MooseX::Aliases::Meta::Trait::Attribute'],
#    },
);

Moose::Exporter->setup_import_methods(
    %metaroles,
);



1;