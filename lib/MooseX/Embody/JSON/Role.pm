package MooseX::Embody::JSON::Role;

use Moose::Role;
use JSON::MaybeXS;

around 'BUILDARGS', sub
{
  my ($orig, $class, @args_l) = @_;
  # if we have single arg and it is not ref, consider it a json string and do decode it
  if (($#args_l == 0) && (ref $args_l[0] eq ""))
  {
    my $json = decode_json($args_l[0]);
    @args_l = %$json;
    return $class->$orig(@args_l);
  }
  my %args;
  if (($#args_l == 0) && (ref $args_l[0] eq "HASH"))
  {
    # if the only arg is hashref then it actually has args
    %args = %{$args_l[0]}
  } else
  {
     %args = @args_l;
  }
  if (exists($args{"json_data"}))
  {
    my $data;
    if (ref($args{"json_data"}) eq '')
    {
      $data = decode_json($args{"json_data"});
    }
    elsif (ref($args{"json_data"}) eq 'HASH')
    {
      $data = $args{"json_data"};
    }
    else
    {
      die "Illegag json_data content type: '".ref($args{"json_data"})."'";
    }
    delete $args{"json_data"};
    foreach my $key (keys %$data)
    {
      die "Duplicate key in json and in BUILDARGS data" if exists $args{$key};
      $args{$key} = $data->{$key};
    }
  }
  return $class->$orig(%args);
};


# FIXME add here check for MooseX::Embody::JSON::Role role.
has '_parent' =>
(
  is => "rw",
  weak_ref => 1
);


# override this sub if you want to perform some action with child object created
sub on_procreate
{
#  my $self = shift;
#  my $child = shift;
}

1;