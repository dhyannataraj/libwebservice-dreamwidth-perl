package MooseX::Embody::JSON;

use Exporter;
use Carp;

use Moose::Util::TypeConstraints;


our @ISA = qw(Exporter);
our @EXPORT = (qw(j_attr j_obj j_hash coerce from via));

# Applying the role as soon as this module was used.
# This will allow to override this role's method with other roles, just after this module have been used.
sub import
{
  my @args = @_;

  my $pkg = (caller)[0];
  _apply_role_once($pkg);
   @_ = @args;
   goto &Exporter::import;
}


sub j_obj
{
  my $name = shift;
  my %args = @_;
  my $class = $args{class};
  delete $args{class};

#eval "require WebService::DreamWidth::Icon";
#"WebService::DreamWidth::Icon"->import();
  no strict qw(refs);
  my $pkg = (caller)[0];  # $pkg now has package name here current subroutine was called.

  confess "You should specify class name defining '$name' at $pkg" unless $class;


  $args{coerce} = 1;
  $args{isa} = $class;
  $args{trigger} = \&_j_obj_updated_trigger;


  &{"${pkg}::coerce"} ( $class,
  &{"${pkg}::from"} ('HashRef'),
  &{"${pkg}::via"} ( sub {"$class"->new($_)})) ;

  &{"${pkg}::has"}($name, %args);  # Thanks to @gordonfish:libera.chat for showing this trick

  _apply_role_once($pkg);
}

sub j_hash
{
  my $name = shift;
  my %args = @_;
  my $class = $args{class};
  delete $args{class};

  no strict qw(refs);
  my $pkg = (caller)[0];  # $pkg now has package name here current subroutine was called.

  confess "You should specify class name defining '$name' at $pkg" unless $class;


  $args{coerce} = 1;
  $args{isa} = "HashRef[$class]";
  $args{trigger} = \&_j_hash_updated_trigger;


  &{"${pkg}::coerce"} ( $class,
  &{"${pkg}::from"} ('HashRef'),
  &{"${pkg}::via"} (
  sub {
    my $h = shift;
    my $res = {};
    foreach my $key (keys %$h)
    {
      $res->{$key} = "$class"->new($h->{$key})
    }
  }));

  &{"${pkg}::has"}($name, %args);  # Thanks to @gordonfish:libera.chat for showing this trick

  _apply_role_once($pkg);
}


sub j_attr
{
  no strict qw(refs);
  my $pkg = (caller)[0];  # $pkg now has package name here current subroutine was called.
#  &{"${pkg}::has"}(@_, is => 'ro'); }
  &{"${pkg}::has"}(@_);  # Thanks to @gordonfish:libera.chat for showing this trick

  _apply_role_once($pkg);
}

sub _apply_role_once
{
  no strict qw(refs);
  no warnings 'once';

  my $pkg = shift;
  if(! defined ${"${pkg}::moosex_embody_json_role_loaded"})
  {
    &{"${pkg}::with"}('MooseX::Embody::JSON::Role'); # This will add all OO related staff to Moose object.
    ${"${pkg}::moosex_embody_json_role_loaded"} = 1;
  }
}

sub _j_obj_updated_trigger
{
  my $self = shift;
  my $new_value = shift;
  if (! $new_value->_parent) # FIXME add better check for attribute not set
  {
    $new_value->_parent($self);
    $self->on_procreate($new_value);
  } else
  {
#    $self->_parent($self); # Why did I ever worote it?
  }
}

sub _j_hash_updated_trigger
{
  my $self = shift;
  my $h = shift;
  foreach my $key (keys %$h)
  {
    if (! $h->{$key}->_parent) # FIXME add better check for attribute not set
    {
      $h->{$key}->_parent($self);
      $self->on_procreate($h->{$key});
    } else
    {
#      $self->_parent($self);  # Why did I ever worote it?
    }
  }
}

1;