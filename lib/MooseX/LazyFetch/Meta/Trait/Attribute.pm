package MooseX::LazyFetch::Meta::Trait::Attribute;

use Moose::Role;

use Carp 'confess';

has lazy_fetch => (
    is         => 'ro',
);

=cut
around 'BUILDARGS', sub
{
    my $orig = shift;
    my $self = shift;
    my @args  =@_;

use Data::Dumper;
print Dumper \@args;

    return ($self->$orig(@_), qw(tmux aha));
};
=cut

around _process_options => sub {
    my $orig = shift;
    my $self = shift;

   my @args = @_;
   my ($name, $options) = @args;
   if ($options->{lazy_fetch})
   {
      foreach my $key (qw(lazy default builder))
      {
        Carp::cluck("Should not use 'lazy_fetch' and '$key' in the same time") if exists $options->{$key};
      }
      $options->{lazy} = 1;
      $options->{default} =
              sub {
                my $obj = shift;
                return undef unless $obj->needs_fetchig(); # If we have alread fetched, and this attr's lazy method have not been called, then it should be undef
                $obj->fetch();
                die "fetch method should set needs_fetching to false" if $obj->needs_fetchig();
                return $obj->meta->get_attribute($name)->get_value($obj);
              };
   }
   return ($self->$orig(@args));
};


around initialize_instance_slot => sub {
    my $orig = shift;
    my $self = shift;
    my ($meta_instance, $instance, $params) = @_;

    my @args = @_;

#use Data::Dumper;
#print Dumper $meta_instance;

    return $self->$orig(@args);
};



1;