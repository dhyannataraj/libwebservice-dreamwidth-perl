package MooseX::LazyFetch::Role;

use Moose::Role;

has 'needs_fetchig' =>
(
  is => 'rw',
  default => 1,
);


sub fetch
{
  my $self = shift;
  my $h = $self->fetcher_implementation();
  $self->_upfetch($h);
  return $self;
}

#update object with fetched data
sub _upfetch
{
  my $self = shift;
  my $h = shift;
  foreach my $attr ($self->meta->get_all_attributes())
  {
    if ($attr->does('MooseX::LazyFetch::Meta::Trait::Attribute'))
    {
      if ($attr->lazy_fetch)
      {
        $attr->set_value($self, $h->{$attr->name}) if exists $h->{$attr->name};
      }
    }
  }
  $self->needs_fetchig(0);
}

sub fetcher_implementation
{

}

1;