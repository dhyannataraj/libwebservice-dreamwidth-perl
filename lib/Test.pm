package Test;

use Moose;
use MooseX::LazyFetch;

has id =>
(
  is=>'rw',
  required => 1,
);


has x =>
(
  is=>'rw',
  lazy_fetch => 1,
);

has y =>
(
  is=>'rw',
  lazy_fetch => 1,
);

has z =>
(
  is=>'rw',
  lazy_fetch => 1,
);




around 'fetcher_implementation' => sub {
    my $orig = shift;
    my $self = shift;

    return {x=>42,y=>3.14,z=>777*$self->id};
#    $self->y(42);

#    return ($self->$orig(@_));
};


1;